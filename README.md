# Camino a Casa

¿Alguna vez has ido de viaje y se te ha olvidado el camino para volver al hotel o el lugar donde te hospedabas? Camino a casa es un juego donde un joven olvidadizo necesita volver a su hogar, pero se ha perdido dentro del bosque (¿) y necesita tu ayuda. ¿Podrás ayudar al niño a llegar a su casa en la menor cantidad de pasos posibles? Inténtalo.

¿Cómo se obtiene el juego?

Para descargar el juego se debe clonar este repositorio y ejecutar python proyecto.py en Linux.
¿Cuáles son sus requisitos?

De preferencia se requiere tener Python versión 3.8.5

¿En que consiste el juego?

El juego consiste en llevar a la persona a su casa siguiendo el camino correcto en el laberinto

¿Cuáles son sus controles?

Los controles del juego son las teclas A,S,D,W las cuales sirven para desplazarse por el mapa
Sobre el programa: 
El programa tiene 5 funcionas las cuales están diseñadas para crear el mapa del laberinto, para mover al jugador y para determinar la posición aleatoria del jugador al empezar el juego.
A medida que el proyecto iba avanzando, se le iba evaluando con flake8 para cumplir con los requisitos del PEP8
El mapa del Laberinto esta creado por emojis al igual que el jugador y la casa.
Al llegar a la meta, se da la opción al usuario de volver a jugar al juego o salir de este.
Las librerías utilizadas fueron OS y RANDOM.

Creado por Martin Castillo 
           Vicente Chandia
           Lucas Casanova.