from random import randint
import os


nivel = """
  xxxxxxxxxxxxxxxxxxxx
x  xxxxxxxxxxxxxxxxxxx
xx xxx x           xxx
xx     x xxxxxxx xxxxx
xx x xxx xxxxxxx xxxxx
xx x      xxxxxx xxxxx
xxxx xxxx x      xx xx
xxxx xxxx x x xx    xx
xxxx xxxx x x xxxxxxxx
xx   xxx  x x x xxxxxx
xxxxxxxxx x x   xxxxxx
xxxx        xxxxxxxxxx
xxxxxx xx xx       xxx
xxx    xx    x x xxxxx
xxx xx xxxxx xxx xxxxx
xxxxxx xxxxx x    xxxx
xxxxxx       x x   xxx
xxxxxx xxxxx   x x xxx
xxxxxx xxxxxxxxx x xxx
xxxxxx xxxxxxxxx x   M
xxxxxxxxxxxxxxxxxxxxxx
"""


ubiX = randint(9, 19)
ubiY = 3
metaX = 21
metaY = 20
cont = 0


def cuadrado(x, y):
    lineas = nivel.split('\n')
    return lineas[y][x]


def dibujar():
    x = 0
    y = 0
    for character in nivel:
        if ubiX == x and ubiY == y:
            print("🏃", end="")
            x = x + 1
        elif character == "x":
            print("🟩", end="")
            x = x + 1
        elif character == "M":
            print("🏠", end="")
            x = x + 1
        elif character == " ":
            print("⬛", end="")
            x = x + 1
        elif character == "\n":
            print(" ")
            x = 0
            y = y + 1


def juego():
    global ubiX
    global ubiY
    global cont
    while True:
        dibujar()
        tecla = input("Ingrese una tecla valida \n")
        if tecla.upper() == "W":
            if cuadrado(ubiX, ubiY - 1) != "x":
                ubiY = ubiY - 1
                cont = cont + 1
        elif tecla.upper() == "S":
            if cuadrado(ubiX, ubiY + 1) != "x":
                ubiY = ubiY + 1
                cont = cont + 1
        elif tecla.upper() == "D":
            if cuadrado(ubiX + 1, ubiY) != "x":
                ubiX = ubiX + 1
                cont = cont + 1
        elif tecla.upper() == "A":
            if cuadrado(ubiX - 1, ubiY) != "x":
                ubiX = ubiX - 1
                cont = cont + 1
        if ubiX == metaX and ubiY == metaY:
            os.system("clear")
            print("¡Llego a casa!")
            print("El número de movimientos realizados es:", cont)
            break


def reiniciar():
    global ubiX
    global ubiY
    global cont
    ubiX = randint(7, 13)
    ubiY = 17
    cont = 0


def menu():
    valido = True
    while valido:
        print("""\n Seleccione una opción:
        1.- volver a jugar
        2.- salir """)
        opcion = input()
        if opcion == "1":
            reiniciar()
            juego()
        elif opcion == "2":
            exit()
        else:
            print("esa opción no es válida, seleccione otra")
            valido = False
    if valido is False:
        menu()

#main


juego()
menu()
